# Expand domain list

This repo has been forked from the [pi-hole-helpers](https://github.com/Kevin-De-Koninck/pi-hole-helpers/) Github from [@Kevin-De-Koninck](https://github.com/Kevin-De-Koninck/), and has been updated by me to work with the latest pihole-FTL version. <br>
I've also updated some scripts to otpimized them.

This will add [The Amazing/Sensational/Remarkable/Revolutionary; #1 Blocklist :)](https://www.reddit.com/r/pihole/comments/bppug1/introducing_the/) from [@sjhgvr](https://reddit.com/user/sjhgvr/) on pihole sub-Reddit. <br>
This will also add a custom whitelist and a custom blacklist that you can modify as many times as you like.

## How-To
```bash
# Method 1.
curl -sSL https://gitlab.com/Natizyskunk/pi-hole-expand-domain-list/raw/master/expand-block-list.sh | bash

# Method 2
wget -O /tmp/expand-block-list.sh https://gitlab.com/Natizyskunk/pi-hole-expand-domain-list/raw/master/expand-block-list.sh
chmod +x /tmp/expand-block-list.sh
sudo /tmp/expand-block-list.sh
sudo rm /tmp/expand-block-list.sh
```
This will take a while so please, be patient :)
